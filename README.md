# Overview

Build the base image to create LFS ISO. This is the fourth step in the LFS pipeline.

Refer to the `docs` project for a general overview of the whole `automated-linux-from-scratch` group.

# Build

Build the image with `docker`, e.g:

```shell script
$ docker build -t base-iso:dev - < Dockerfile
```

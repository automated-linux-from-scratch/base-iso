FROM archlinux:20200908
LABEL maintainer="Aleksander Ciołek <aleksander.ciolek@protonmail.com>"

RUN pacman -Syu --noconfirm --quiet \
    busybox \
    cpio \
    docker \
    grub \
    libisoburn \
    mtools \
    squashfs-tools

CMD ["/bin/bash"]
